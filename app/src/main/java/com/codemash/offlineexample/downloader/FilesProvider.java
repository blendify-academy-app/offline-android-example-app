package com.codemash.offlineexample.downloader;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.ContextCompat;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class FilesProvider {

    private Activity _activity = null;

    private String _folder = "eitdigitalx";

    private Map<String,Integer> _queue = null;

    private FilesProviderListener _listener = null;

    public FilesProvider(Activity activity) {
        this._activity = activity;

        this._queue = new HashMap<>();
    }

    public void setListener(FilesProviderListener listener) {
        this._listener = listener;
    }

    private boolean hasStoragePermissionGranted(){
        return ContextCompat.checkSelfPermission(this._activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    public void checkExternalStoragePermissions() {
        if (!this.hasStoragePermissionGranted()) {
            this.requestExternalStoragePermission();
        }
    }

    private void requestExternalStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            this._activity.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }

    public File getFile(String key) {
        try {
            String token = FileDownloader.fileNameFromKey(key);
            File folder = new File(Environment.getExternalStorageDirectory() + "/" + this._folder);
            File[] resources = folder.listFiles();
            for (int i = 0; i < resources.length; i++) {
                if (resources[i].isFile()) {
                    String name = resources[i].getName();
                    int pos = name.lastIndexOf(".");
                    if (pos > 0) {
                        String matcher = name.substring(0, pos);
                        if (matcher.equals(token)) {
                            return resources[i];
                        }
                    }
                }
            }
        } catch (Exception ex) {}

        return null;
    }

    public void downloadFileInQueue(String url, String key, String ext) {
        if (this.getFile(key) != null) {
            if (this._listener != null) {
                this._listener.onFileReady(key);
            }
            return;
        }

        FileDownloader fileDownloader = new FileDownloader(this._folder);

        this._queue.put(url, 0);

        fileDownloader.downloadFromURL(url, key, ext, new DownloadFileListener() {
            @Override
            public void onProgressUpdated(String key, int progress) {
                _queue.put(key, progress);
                queueUpdated();
            }

            @Override
            public void onFileDownloaded(String key, String path) {
                _queue.put(key, 100);
                if (_listener != null) {
                    _listener.onFileReady(key);
                }
            }
        });
    }

    public void triggerFileAction(String key) {
        File file = getFile(key);
        if (file == null) {
            return;
        }

        String filePath = file.getAbsolutePath();
        String ext = filePath.substring(filePath.lastIndexOf(".") + 1, filePath.length());

        switch (ext) {
            case "mp4":
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(file), "video/mp4");
                this._activity.startActivity(intent);
                break;
        }
    }

    private void queueUpdated() {
        int count = 0;
        int total = 0;
        int currentlyDownloading = 0;

        for (Integer progress : this._queue.values()) {
            count++;
            total += progress;

            if (progress != 100) {
                currentlyDownloading++;
            }
        }

        int average = (int) ((double) total / (double) count);

        if (this._listener != null) {
            this._listener.onProgressUpdated(average, currentlyDownloading);
        }
    }
}
