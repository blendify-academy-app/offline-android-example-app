package com.codemash.offlineexample;

import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import com.codemash.offlineexample.downloader.FilesProvider;
import com.codemash.offlineexample.downloader.FilesProviderListener;

public class JavascriptBridge {

    private DatabaseManager _databaseManager = null;

    private FilesProvider _filesProvider = null;

    private WebView _webView = null;

    public JavascriptBridge(WebView webView, DatabaseManager databaseManager, FilesProvider filesProvider) {
        this._webView = webView;
        this._databaseManager = databaseManager;
        this._filesProvider = filesProvider;

        this._filesProvider.setListener(new FilesProviderListener() {
            @Override
            public void onProgressUpdated(final int progress, final int currentlyDownloading) {
                _webView.post(new Runnable() {
                    @Override
                    public void run() {
                        _webView.loadUrl("javascript:window.androidWebview.onProgressUpdated(" + progress + "," + currentlyDownloading + ")");
                    }
                });
            }

            @Override
            public void onFileReady(final String key) {
                _webView.post(new Runnable() {
                    @Override
                    public void run() {
                        _webView.loadUrl("javascript:window.androidWebview.onFileReady('" + key + "')");
                    }
                });
            }
        });
    }

    @JavascriptInterface
    public String getURL() {
        return _databaseManager.getURL();
    }

    @JavascriptInterface
    public void downloadFile(String url, String key, String ext) {
        _filesProvider.downloadFileInQueue(url, key, ext);
    }

    @JavascriptInterface
    public void triggerFileAction(String key) {
        _filesProvider.triggerFileAction(key);
    }

    @JavascriptInterface
    public void clearCache() {
        _webView.post(new Runnable() {
            @Override
            public void run() {
                _webView.clearCache(true);
            }
        });
    }
}
