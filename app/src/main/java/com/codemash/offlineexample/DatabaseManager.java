package com.codemash.offlineexample;

import android.content.Context;

import com.couchbase.lite.Database;
import com.couchbase.lite.Manager;
import com.couchbase.lite.View;
import com.couchbase.lite.android.AndroidContext;
import com.couchbase.lite.javascript.JavaScriptReplicationFilterCompiler;
import com.couchbase.lite.javascript.JavaScriptViewCompiler;
import com.couchbase.lite.listener.Credentials;
import com.couchbase.lite.listener.LiteListener;
import com.couchbase.lite.router.URLStreamHandlerFactory;
import com.couchbase.lite.util.Log;

import java.io.IOException;
import java.util.Locale;

public class DatabaseManager {

    private Credentials _credentials;

    private Manager _manager;

    private Context _context;

    private int _port;

    public DatabaseManager(Context context, int port) {
        this._context = context;
        this._port = port;
    }

    public void startDatabase() {
        this._credentials = new Credentials("", "");

        URLStreamHandlerFactory.registerSelfIgnoreError();

        View.setCompiler(new JavaScriptViewCompiler());
        Database.setFilterCompiler(new JavaScriptReplicationFilterCompiler());

        this._manager = this.getDatabaseManager(this._context);

        runListener(this._port, this._manager, this._credentials);
    }

    private void runListener(int port, Manager manager, Credentials credentials) {
        LiteListener listener = new LiteListener(manager, port, credentials);

        // Overwrite port.
        this._port = listener.getListenPort();

        // Run the listener.
        Thread thread = new Thread(listener);
        thread.start();
    }

    private Manager getDatabaseManager(Context context) {
        Manager manager;
        try {
            Manager.enableLogging(Log.TAG, Log.VERBOSE);
            Manager.enableLogging(Log.TAG_SYNC, Log.VERBOSE);
            Manager.enableLogging(Log.TAG_QUERY, Log.VERBOSE);
            Manager.enableLogging(Log.TAG_VIEW, Log.VERBOSE);
            Manager.enableLogging(Log.TAG_CHANGE_TRACKER, Log.VERBOSE);
            Manager.enableLogging(Log.TAG_BLOB_STORE, Log.VERBOSE);
            Manager.enableLogging(Log.TAG_DATABASE, Log.VERBOSE);
            Manager.enableLogging(Log.TAG_LISTENER, Log.VERBOSE);
            Manager.enableLogging(Log.TAG_MULTI_STREAM_WRITER, Log.VERBOSE);
            Manager.enableLogging(Log.TAG_REMOTE_REQUEST, Log.VERBOSE);
            Manager.enableLogging(Log.TAG_ROUTER, Log.VERBOSE);
            manager = new Manager(new AndroidContext(context), Manager.DEFAULT_OPTIONS);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return manager;
    }

    public String getURL() {
        // This will be exposed to JS through the bridge.
        return String.format(Locale.ENGLISH, "http://localhost:%d/", this._port);
    }

}
