package com.codemash.offlineexample.downloader;

import android.content.Context;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class FileDownloader {

    private Context _activity = null;

    private String _folder = null;

    public FileDownloader(String folder) {
        this._folder = folder;
    }

    public static String fileNameFromKey(String key) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        final MessageDigest digest = MessageDigest.getInstance("SHA-1");
        byte[] result = digest.digest(key.toString().getBytes("UTF-8"));
        StringBuilder sb = new StringBuilder();

        for (byte b : result) {
            sb.append(String.format("%02X", b));
        }

        return sb.toString();
    }

    public void downloadFromURL(String url, String key, String ext, DownloadFileListener listener) {
        new DownloadFileAsync(listener).execute(url, key, ext, this._folder);
    }

}