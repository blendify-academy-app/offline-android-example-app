package com.codemash.offlineexample.downloader;

public interface DownloadFileListener {
    void onProgressUpdated(String key, int progress);
    void onFileDownloaded(String key, String path);
}
