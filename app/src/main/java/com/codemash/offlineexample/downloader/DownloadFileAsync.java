package com.codemash.offlineexample.downloader;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class DownloadFileAsync extends AsyncTask<String, String, String> {

    private String _folder = null;

    private URL _url = null;

    private int _progress = 0;

    private String _ext = null;

    private String _key = null;

    protected DownloadFileListener _listener = null;

    public DownloadFileAsync(DownloadFileListener listener) {
        this._listener = listener;
    }

    @Override
    protected String doInBackground(String... params) {
        this._key = params[1];
        this._ext = params[2];
        this._folder = params[3];

        try {
            this._url = new URL(params[0]);
            URLConnection connection = this._url.openConnection();
            connection.connect();

            int lengthOfFile = connection.getContentLength();

            InputStream input = new BufferedInputStream(this._url.openStream());

            File video = new File(Environment.getExternalStorageDirectory() + "/" + this._folder + "/" + FileDownloader.fileNameFromKey(this._key.toString()) + '.' + this._ext);

            // Create the folder.
            if (!video.getParentFile().exists()) {
                video.getParentFile().mkdirs();
            }

            // This is for debug purposes at the moment.
            if (video.exists()) {
                video.delete();
            }

            video.createNewFile();
            OutputStream output = new FileOutputStream(video);

            byte data[] = new byte[4096];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                total += count;
                this._progress = (int) (total * 100 / lengthOfFile);
                output.write(data, 0, count);

                if (this._listener != null) {
                    this._listener.onProgressUpdated(this._url.toString(), this._progress);
                }
            }

            output.flush();
            output.close();
            input.close();

            if (this._listener != null) {
                this._listener.onFileDownloaded(this._key.toString(), video.getAbsolutePath().toString());
            }
        } catch (Exception ex) {
            Log.i("DownloadFileAsyncError", ex.getMessage());
        }

        return null;
    }

}