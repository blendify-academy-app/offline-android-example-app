package com.codemash.offlineexample;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import com.codemash.offlineexample.downloader.FilesProvider;

public class MainActivity extends AppCompatActivity {

    private String _testsUrl = "http://uwsoftware.be/someTest/";

    private String _url = "http://pilot-2215.dev.imindsx.org/offline/2";

    private DatabaseManager _databaseManager = null;

    private FilesProvider _filesDownloader = null;

    static final boolean NO_CACHE = false;

    private int _dbPort = 5984;

    private WebView _webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            StrictMode.VmPolicy.Builder build = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(build.build());
        }

        this._filesDownloader = new FilesProvider(this);
        this._filesDownloader.checkExternalStoragePermissions();

        setContentView(R.layout.activity_main);

        this._databaseManager = new DatabaseManager(this, this._dbPort);
        this._databaseManager.startDatabase();

        this.startWebview();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void startWebview() {
        this._webView = (WebView) this.findViewById(R.id.webView);

        this._webView.getSettings().setJavaScriptEnabled(true);

        String appCachePath = getApplicationContext().getCacheDir().getAbsolutePath();
        this._webView.getSettings().setAppCachePath(appCachePath);
        this._webView.getSettings().setAppCacheEnabled(true);
        this._webView.getSettings().setAllowFileAccess(true);
        this._webView.getSettings().setDomStorageEnabled(true);

        this._webView.setWebChromeClient(new WebChromeClient());
        this._webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);

        if (this.NO_CACHE) {
            this._webView.getSettings().setAppCacheEnabled(false);
            this._webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        }

        if (!isNetworkAvailable() && !this.NO_CACHE) {
            Toast toast = Toast.makeText(getApplicationContext(), "Loading offline version.", Toast.LENGTH_SHORT);
            toast.show();
        }

        this._webView.setWebViewClient(new WebViewClient());

        this._webView.addJavascriptInterface(new JavascriptBridge(this._webView, this._databaseManager, this._filesDownloader), "androidWebview");

        this._webView.loadUrl(this._url);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_pilot:
                this._webView.loadUrl(this._url);
                return true;
            case R.id.action_test:
                this._webView.loadUrl(this._testsUrl);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStop() {
        // Workaround for kitkat bug: https://bugs.chromium.org/p/chromium/issues/detail?can=2&start=0&num=100&q=&colspec=ID%20Pri%20M%20Iteration%20ReleaseBlock%20Cr%20Status%20Owner%20Summary%20OS%20Modified&groupby=&sort=&id=258191
        finish();
        super.onStop();
    }
}
