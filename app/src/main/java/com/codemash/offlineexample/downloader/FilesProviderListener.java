package com.codemash.offlineexample.downloader;

public interface FilesProviderListener {
    void onProgressUpdated(int progress, int currentlyDownloading);
    void onFileReady(String key);
}
